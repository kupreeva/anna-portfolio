from django.shortcuts import render, redirect
from django.views.generic.base import TemplateView
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse
from portfolio.forms import ContactForm

# Create your views here.


class HomePageView(TemplateView):
    template_name = "portfolio/index.html"


class AboutPageView(TemplateView):
    template_name = "portfolio/about.html"


def contact_form_view(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            subject = 'From {} {}'.format(first_name, last_name)
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(subject, message, from_email, ['kristine.kupreeva@gmail.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return render(request, "portfolio/contact.html", {'form': ContactForm()})
    return render(request, "portfolio/contact.html", {'form': form})


# albums


class SeasonsAlbumView(TemplateView):
    template_name = "portfolio/seasons.html"


class PortraitsAlbumView(TemplateView):
    template_name = "portfolio/portraits.html"


class CitiesPeopleAlbumView(TemplateView):
    template_name = "portfolio/citiespeople.html"


class ReportageAlbumView(TemplateView):
    template_name = "portfolio/reportag.html"


class NatureAlbumView(TemplateView):
    template_name = "portfolio/nature.html"


class MacroAlbumView(TemplateView):
    template_name = "portfolio/macro.html"
