from django.urls import path
from portfolio import views

urlpatterns = [
    path('about/', views.AboutPageView.as_view(), name='about'),
    path('contact/', views.contact_form_view, name='contact'),
    path('', views.HomePageView.as_view(), name='home'),

    # albums
    path('album/seasons/', views.SeasonsAlbumView.as_view(), name='seasons'),
    path('album/portraits', views.PortraitsAlbumView.as_view(), name='portraits'),
    path('album/cities_and_people', views.CitiesPeopleAlbumView.as_view(), name='cities_people'),
    path('album/photo_session/', views.ReportageAlbumView.as_view(), name='reportage'),
    path('album/nature/', views.NatureAlbumView.as_view(), name='nature'),
    path('album/macro/', views.MacroAlbumView.as_view(), name='macro'),

]