from django import forms


class ContactForm(forms.Form):
    from_email = forms.EmailField(required=True, widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true',
    }))
    first_name = forms.CharField(required=True, widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true'
    }))
    last_name = forms.CharField(required=True, widget=forms.TextInput(attrs={
        'class': 'form-control',
        'required': 'true'
    }))
    message = forms.CharField(required=True, widget=forms.Textarea(attrs={
            'class': 'form-control',
            'required': 'true',
            'cols': "30",
            'rows': "10",
    }))
